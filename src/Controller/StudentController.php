<?php

namespace App\Controller;

use App\Form\StudentForm;
use App\Entity\Student;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Cloudinary\ImageUploader;
use App\Manager\CredentialManager;
use App\Manager\QRManager;
use App\Manager\MailerManager;
use DateTime;

class StudentController extends AbstractController
{
    /**
     * @Route("/students", name="students_homepage")
     */
    public function index()
    {
        return $this->render("base.html.twig");
    }

    /**
     * @Route("/students/new", name="new_student")
     */
    public function newStudent(Request $request, EntityManagerInterface $em, ImageUploader $uploadCloudinary) 
    {
        $form = $this->createForm(StudentForm::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $student = $form->getData();
            $studentImg = $form->get('avatarImg')->getData();

            $em->persist($student);
            $em->flush();

            $path = $this->getParameter('kernel.project_dir').'/public/images';
            $filename = $student->getName().'.'.$studentImg->guessClientExtension();

            $studentImg->move(
                $path,
                $filename
            );

            $cloudinaryResponse = $uploadCloudinary->uploadImageToCloudinary("$path/$filename");

            $student->setAvatar($cloudinaryResponse);

            $studentVM = $form->get('validMonth')->getData();
            $limitDate = new DateTime('now');
            $limitDate->modify("+$studentVM months");
            $student->setValidity($limitDate);

            $em->flush();

            unlink("$path/$filename");

            return $this->redirectToRoute('generate_credential', ["id"=>$student->getId()]);
        }
        return $this->render('student/new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/students/edit/{id}", name="edit_student")
     */
    public function editStudent(Student $student, Request $request, EntityManagerInterface $em, ImageUploader $uploadCloudinary) 
    {
        $form = $this->createForm(StudentForm::class, $student);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $student = $form->getData();
            $studentImg = $form->get('avatarImg')->getData();

            if($studentImg)
            {
                $path = $this->getParameter('kernel.project_dir').'/public/images';
                $filename = $student->getName().'.'.$studentImg->guessClientExtension();
    
                $studentImg->move(
                    $path,
                    $filename
                );
    
                $cloudinaryResponse = $uploadCloudinary->uploadImageToCloudinary("$path/$filename");
    
                $student->setAvatar($cloudinaryResponse);
            }

            $studentVM = $form->get('validMonth')->getData();
            $limitDate = new DateTime('now');
            $limitDate->modify("+$studentVM months");
            $student->setValidity($limitDate);

            $em->persist($student);
            $em->flush();

            return $this->redirectToRoute('all_students');
        }
        return $this->render('student/new.html.twig', ['form' => $form->createView(), "student" => $student]);
    }

    /**
     * @Route("/students/all", name="all_students")
     */
    public function listAllStudents(EntityManagerInterface $em)
    {
        $repo = $em->getRepository(Student::class);
        $students = $repo->findAll();

        return $this->render('student/list.html.twig', ['students' => $students]);
    }

    /**
     * @Route("/students/delete/{id}", name="delete_student")
     */
    public function deleteStudent($id, EntityManagerInterface $em)
    {
        $repo = $em->getRepository(Student::class);
        $student = $repo->find($id);

        $em->remove($student);
        $em->flush();

        return $this->redirectToRoute("all_students");
    }

    /**
     * @Route("/students/{id}/generate-credential/", name="generate_credential")
     */
    public function generateCredential($id, EntityManagerInterface $em, CredentialManager $cManager, QRManager $qrManager, MailerManager $mailer) 
    {
        $repo = $em->getRepository(Student::class);
        $student = $repo->find($id);

        $path = $this->getParameter('kernel.project_dir').'/public/images';

        $resultqr = $qrManager->generateQR($id);

        $cManager->printInfo($student, $resultqr);
        $credentialSrc = "$path/credentials/$id.jpg";
        
        unlink("$path/qr/$id.png");

        $email = $student->getEmail();
        $mailer->sendEmail($credentialSrc, $email);

        return $this->redirectToRoute("homepage");
    }

    /**
     * @Route("/students/{id}", name="get_student")
     */
    public function getStudent($id, EntityManagerInterface $em) 
    {
        $repo = $em->getRepository(Student::class);
        $student = $repo->find($id);
        $studentValidityDate = $student->getValidity();
        $dateNow = new DateTime('now');

        $flagValidity = false;

        if($dateNow < $studentValidityDate) {
            $flagValidity = true;
        }

        return $this->render("student/profile.html.twig", ['student' => $student, 'flag' => $flagValidity]);
    }

}