<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->redirectToRoute("students_homepage");
    }

    /**
     * @Route("/create/admin", name="create_admin")
     */
    public function createAdmin(EntityManagerInterface $em, UserPasswordHasherInterface $hash)
    {
        $admin = new User;
        $admin->setUsername('admin');
        $admin->setRoles(['ROLE_ADMIN']);

        $password = $hash->hashPassword($admin, '1234');
        $admin->setPassword($password);

        $em->persist($admin);
        $em->flush();
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/remove/{id}", name="remove_admin")
     */
    public function deleteAdmin($id, EntityManagerInterface $em)
    {
        $repo = $em->getRepository(User::class);
        $user = $repo->find($id);

        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute("homepage");
    }
}