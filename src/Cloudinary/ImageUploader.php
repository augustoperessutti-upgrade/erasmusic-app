<?php
namespace App\Cloudinary;

use Cloudinary\Api\Upload\UploadApi;
use Cloudinary\Configuration\Configuration;

class ImageUploader
 {
     public function uploadImageToCloudinary(string $file)
     {
        $cloudName = $_ENV['CLOUD_NAME'];
        $cloudKey = $_ENV['CLOUD_KEY'];
        $cloudSecret = $_ENV['CLOUD_SECRET'];

        Configuration::instance([
        'cloud' => [
            'cloud_name' => "$cloudName", 
            'api_key' => "$cloudKey", 
            'api_secret' => "$cloudSecret"],
        'url' => [
            'secure' => true]
        ]);

        $imageUploaded = (new UploadApi())->upload($file);
 
         return $imageUploaded['secure_url'];
     }
}