<?php

namespace App\Manager;

use Mael\InterventionImageBundle\MaelInterventionImageManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Intl\Intl;

\Locale::setDefault('en');

class CredentialManager extends AbstractController
{
    protected $imageManager;

    public function __construct(MaelInterventionImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    public function printInfo($studentInfo, $qr)
    {
        $path = $this->getParameter('kernel.project_dir').'/public';
        
        $stId = $studentInfo->getId();
        $stName = $studentInfo->getName();
        $stSurname = $studentInfo->getSurname();

        $stValidity = $studentInfo->getValidity();
        $validityMonth = $stValidity->format('m');
        $validityYear = $stValidity->format('Y');

        $stCountry = $studentInfo->getCountry();
        $country = Countries::getName($stCountry);

        $stImg = $studentInfo->getAvatar();

        $newStImg = $this->imageManager->make($stImg)
            ->resize(477, null, function ($constraint){
                $constraint->aspectRatio();
            })
            ->crop(477, 477);

        $respuesta = $this->imageManager->make("$path/images/credentials/base-erasmusic.png")
            ->insert($newStImg, 'center', 0, -518)
            ->insert("$path/images/credentials/base-erasmusic.png")
            ->text($stName, 94, 910, function($font) {
                $font->file($this->getParameter('kernel.project_dir').'/public/fonts/Montserrat.otf');
                $font->size(40);
                $font->color('#141414');
            })
            ->text($stSurname, 514, 910, function($font) {
                $font->file($this->getParameter('kernel.project_dir').'/public/fonts/Montserrat.otf');
                $font->size(40);
                $font->color('#141414');
            })
            ->text($country, 94, 1060, function($font) {
                $font->file($this->getParameter('kernel.project_dir').'/public/fonts/Montserrat.otf');
                $font->size(40);
                $font->color('#141414');
            })
            ->text("Hasta $validityMonth/$validityYear", 514, 1060, function($font) {
                $font->file($this->getParameter('kernel.project_dir').'/public/fonts/Montserrat.otf');
                $font->size(40);
                // $font->align('center');
                $font->color('#141414');
            })
            ->insert($qr, 'center', 0, 520)
            ->save("$path/images/credentials/$stId.jpg");

        return $respuesta;
    }
}