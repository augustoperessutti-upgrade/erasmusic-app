<?php

namespace App\Manager;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class QRManager extends AbstractController
{
    public function generateQR($id)
    {
        $path = $this->getParameter('kernel.project_dir').'/public';
        $writer = new PngWriter();

        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

        $qrCode = QrCode::create($actual_link."/students/$id")
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));
        
        $result = $writer->write($qrCode);
        $result->saveToFile("$path/images/qr/$id.png");
        return $result->getString();

    }
}