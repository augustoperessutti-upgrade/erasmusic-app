<?php

namespace App\Manager;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailerManager extends AbstractController
{
    protected $mailer;
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }
    public function sendEmail($image, $mail)
    {
        $email = (new Email())
            ->from('augustoperessutti.erasmusic@gmail.com')
            ->to($mail)
            ->subject('Aquí esta tu tarjeta Erasmusic!')
            ->text('Descarga tu tarjeta y empieza a disfrutar de los beneficios!')
            ->attachFromPath($image);
            // ->html('<p>See Twig integration for better HTML integration!</p>');

        $this->mailer->send($email);
    }
}