<?php

namespace App\Form;

use App\Entity\Student;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\{ CountryType, ChoiceType, BirthdayType, EmailType, FileType, SubmitType };
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('country', CountryType::class,[
                'preferred_choices' => ['en', 'es'],
                'placeholder' => 'Elige un país',
            ])
            ->add('birth', BirthdayType::class, [
                'placeholder' => [
                    'year' => 'Año', 'month' => 'Mes', 'day' => 'Día',
                ],
                'format' => 'ddMMyyyy'
            ])
            ->add('phone')
            ->add('email', EmailType::class)
            ->add('avatarImg', FileType::class, [
                'required' => false,
                "mapped" => false,
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    "Hombre" => 'male',
                    "Mujer" => 'female',
                    "Otro" => 'other'
                ],
                'placeholder' => 'Elige uno'
            ])
            ->add('instagram')
            ->add('facebook')
            ->add('validMonth', ChoiceType::class, [
                'choices' => [
                    "1" => '1',
                    "2" => '2',
                    "3" => '3',
                    "4" => '4',
                    "5" => '5',
                    "6" => '6',
                    "7" => '7',
                    "8" => '8',
                    "9" => '9',
                    "10" => '10',
                    "11" => '11',
                    "13" => '13'
                ],
                'mapped' => false
            ])
            ->add('submit', SubmitType::class, ['label' => 'Enviar']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Student::class,
        ]);
    }

}